import base64
import os
from datetime import datetime
from threading import Lock

import cv2
import numpy as np
from django import shortcuts
from django.conf import settings
from django.http import JsonResponse

from common import utils

base_dir = settings.BASE_DIR  # datalab-main

path_projects = os.path.join(base_dir, 'projects')


def render(request):
    return shortcuts.render(request, 'projects/visu/yolo.html', {})


def object_detection(request):
    if request.method == 'POST' and request.is_ajax():
        start = datetime.now()

        img = base64.b64decode(request.POST['img'].replace('data:image/jpeg;base64,', ''))
        coord_labels = process_image(img)

        print("Processing time :", datetime.now() - start)
    return JsonResponse({"coord_labels": coord_labels})


configs = {"coco": {"names": "Coco.names", "cfg": "YOLOv3_608.cfg", "weights": "YOLOv3_608.weights"}}
nets = {}
lock = Lock()


def load_labels(filename):
    file = open(filename, 'r')
    text = file.read().strip().split("\n")
    file.close()
    return text


def get_layers_name(network):
    ln = network.getLayerNames()
    ln = [ln[i[0] - 1] for i in network.getUnconnectedOutLayers()]
    return ln


def get_net(name):
    if name in nets:
        return nets[name]

    config = configs[name]
    root_project_folder = os.path.join(os.path.dirname(os.path.abspath(__file__)))
    yolo_path = os.path.join(root_project_folder, "yolo")
    labelspath = os.path.join(yolo_path, config["names"])

    net = utils.Bunch()

    net.labels = load_labels(labelspath)

    # initialize a list of colors to represent each possible class label
    np.random.seed(10)
    net.colors = np.random.randint(0, 255, size=(len(net.labels), 3), dtype="uint8")

    # Path to Yolo weight and file configuration
    weightpath = os.path.join(yolo_path, config["weights"])
    configpath = os.path.join(yolo_path, config["cfg"])

    # Load object detection model trained on coco dataset from the disk
    net.network = cv2.dnn.readNetFromDarknet(configpath, weightpath)
    net.ln = get_layers_name(net.network)
    nets[name] = net
    return net


def process_image(img, net_name = "coco", conf=0.5, threshold=0.3):
    lock.acquire()
    try:
        net = get_net(net_name)
        nparr = np.fromstring(img, np.uint8)
        image = cv2.imdecode(nparr, cv2.cv2.IMREAD_COLOR)

        # A décommenter si on travaille sur une image locale
        # image = cv2.imread(img)

        # image = cv2.resize(image, None, fx=0.4, fy=0.4)
        H, W, C = image.shape

        # Processing image
        ''' cv2.dnn.blobFromImage and cv2.dnn.blobFromImages are two functions of (dnn) 
           module that can be used for preprocessing images and preparing them for classification
             via pre-trained deep learning models. These two functions perform : Mean subtraction, Scaling, And optionally channel swapping'''

        blob = cv2.dnn.blobFromImage(image, 1 / 255, (416, 416), (0, 0, 0), True, crop=False)

        net.network.setInput(blob)
        outputlayers = net.network.forward(net.ln)

        # visualization of detection on screen

        ''' Initialization of list of detected bounding box, confidences and classes IDS'''

        boxes = []
        confidences = []
        classIDs = []

        for output in outputlayers:
            for detection in output:
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]
                '''filter out weak predictions by ensuring the detected
                probability is greater than the minimum probability'''

                if confidence > conf:
                    '''scale the bounding box coordinates back relative to the size of the image'''

                    box = detection[:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box

                    x = int(centerX - width / 2)
                    y = int(centerY - height / 2)

                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)

        # Apply Non max suppression

        idxs = cv2.dnn.NMSBoxes(boxes, confidences, conf, threshold)

        #  draw the boxes and class text on the image
        listLabels = []
        i_obj = 0
        if len(idxs) > 0:
            for i in idxs.flatten():
                obj = {}
                x, y, w, h = boxes[i]
                color = [int(c) for c in net.colors[classIDs[i]]]
                text = net.labels[classIDs[i]].capitalize()
                obj["class"] = text
                obj["color"] = color
                obj["x"] = x
                obj["y"] = y
                obj["w"] = w
                obj["h"] = h
                listLabels.append(obj)
                i_obj += 1

        return listLabels
    finally:
        lock.release()
