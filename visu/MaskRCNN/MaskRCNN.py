import os
import base64
import os
from datetime import datetime
from threading import Lock
import cv2
import numpy as np
import time
import random

import cv2
import numpy as np
from django import shortcuts
from django.conf import settings
from django.http import JsonResponse

from common import utils

confThres = 0.5  # Confidence threshold
maskThres = 0.3  # Mask threshold

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
labels_paths = os.path.join(BASE_DIR, "MaskRCNN/mscoco_labels.names")


# get labels names


def load_labels(filename):
    file = open(filename, 'r')
    text = file.read().strip().split("\n")
    file.close()
    return text


labels = load_labels(labels_paths)

# initialize a list of colors to represent each possible class label
''' Random colors'''
np.random.seed(42)
COLORS_1 = np.random.randint(0, 255, size=(len(labels), 3), dtype="uint8")

''' colors personalisation '''


def get_color_perso(filename):
    file = open(filename, 'r')
    color_value = file.read().strip().split("\n")
    file.close()
    colors = []
    for i in range(len(color_value)):
        rgb = color_value[i].split(' ')
        color = np.array([float(rgb[0]), float(rgb[1]), float(rgb[2])])
        colors.append(color)
    return colors


COLORS_PATH = os.path.join(BASE_DIR, "MaskRCNN/Mask_rcnn_coco/colors.txt")

COLORS_2 = get_color_perso(COLORS_PATH)

# Give the textGraph and weight files for the model

configpath = os.path.join(BASE_DIR, "MaskRCNN/Mask_rcnn_coco/mask_rcnn_inception_v2_coco_2018_01_28.pbtxt")
weightpath = os.path.join(BASE_DIR, "MaskRCNN/Mask_rcnn_coco/mask_rcnn_inception_config.pb")

# Load the network
# Reads a network model stored in TensorFlow framework format.
net = cv2.dnn.readNetFromTensorflow(weightpath, configpath)
# Ask network to use specific computation backend where it supported
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
# Ask network to make computations on specific target device.
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

# Read input or load image

image_path = os.path.join(BASE_DIR, "images/sans-titre2.png")

# get box and mask of object trought forward  pass


image = cv2.imread(image_path)
H, W = image.shape[:2]
blob = cv2.dnn.blobFromImage(image, swapRB=True, crop=False)
net.setInput(blob)
start = time.time()
(boxes, masks) = net.forward(["detection_out_final", "detection_masks"])


start = time.time()


# print(masks.shape, boxe.shape)
end = time.time()

print("[INFO] Mask R-CNN took {:.6f} seconds".format(end - start))
print("[INFO] boxes shape: {}".format(boxes.shape))
print("[INFO] masks shape: {}".format(masks.shape))

print(boxes[0,0,1])
print(masks[1])
#for conf in boxes[0,0,:,:]:
    #confidence = conf[2]
   # print(confidence)

# loop over the number of object detected
for i in range(0, boxes.shape[2]):
    classID = int(boxes[0, 0, i, 1])
    confidence = boxes[0, 0, i, 2]
    if confidence >0.5:
        clone = image.copy()
        box = boxes[0, 0, i, 3:7] * np.array([W, H, W, H])
        (startX, startY, endX, endY) = box.astype("int")
        boxW = endX - startX
        boxH = endY - startY

        mask = masks[i, classID]
        mask = cv2.resize(mask, (boxW, boxH),
                          interpolation=cv2.INTER_NEAREST)
        mask = (mask > 0.3)
        roi = clone[startY:endY, startX:endX]
        roi = roi[mask]
        # print(roi)
        color = random.choice(COLORS_2)
        blended = ((0.4 * color) + (0.6 * roi)).astype("uint8")
        clone[startY:endY, startX:endX][mask] = blended
        color = [int(c) for c in color]

        cv2.rectangle(clone, (startX, startY), (endX, endY), color, 2)

        text = "{}: {:.4f}".format(labels[classID], confidence)
        cv2.putText(clone, text, (startX, startY - 5),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

        cv2.imshow("Output", clone)
        cv2.waitKey(0)

