import os
import base64
import os
from datetime import datetime
from threading import Lock
import cv2
import numpy as np
import time
import random
from matplotlib import pyplot as plt
import cv2
import numpy as np
from django import shortcuts
from django.conf import settings
from django.http import JsonResponse

from common import utils

confThres = 0.5  # Confidence threshold
maskThres = 0.3  # Mask threshold

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# get labels names and  personnalise colors

def load_labels_colors(path):
    labels_paths = os.path.join(path, "MaskRCNN/mscoco_labels.names")
    COLORS_PATH = os.path.join(path, "MaskRCNN/Mask_rcnn_coco/colors.txt")
    labels_file = open(labels_paths, 'r')
    text = labels_file.read().strip().split("\n")
    labels_file.close()

    color_file = open(COLORS_PATH, 'r')
    color_value = color_file.read().strip().split("\n")
    color_file.close()
    colors = []
    for i in range(len(color_value)):
        rgb = color_value[i].split(' ')
        color = np.array([float(rgb[0]), float(rgb[1]), float(rgb[2])])
        colors.append(color)
    return text, colors


labels, COLORS_2 =  load_labels_colors(BASE_DIR)

# initialize a list of colors to represent each possible class label
''' Random colors'''
np.random.seed(42)
COLORS_1 = np.random.randint(0, 255, size=(len(labels), 3), dtype="uint8")
''' colors personalisation '''

def get_masrcnn_net(path):
    # Give the textGraph and weight files for the model
    configpath = os.path.join(path, "MaskRCNN/Mask_rcnn_coco/mask_rcnn_inception_v2_coco_2018_01_28.pbtxt")
    weightpath = os.path.join(path, "MaskRCNN/Mask_rcnn_coco/mask_rcnn_inception_config.pb")
    # Load the network and Reads a network model stored in TensorFlow framework format.
    net = cv2.dnn.readNetFromTensorflow(weightpath, configpath)
    # Ask network to use specific computation backend where it supported
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
    # Ask network to make computations on specific target device.
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
    return net


net = get_masrcnn_net(BASE_DIR)


# Read input or load image

image_path = os.path.join(BASE_DIR, "images/sans-titre2.png")

# get box and mask of object trought forward  pass


def make_predict(path):
    image_path = os.path.join(path, "images/sans-titre2.png")
    image = cv2.imread(image_path)
    H, W = image.shape[:2]
    blob = cv2.dnn.blobFromImage(image, swapRB=True, crop=False)
    net.setInput(blob)
    (boxes, masks) = net.forward(["detection_out_final", "detection_masks"])
    return boxes, masks, H, W


start = time.time()
boxes, masks, H, W = make_predict(BASE_DIR)
# print(masks.shape, boxes.shape)
end = time.time()
print("[INFO] Mask R-CNN took {:.6f} seconds".format(end - start))
print("[INFO] boxes shape: {}".format(boxes.shape))
print("[INFO] masks shape: {}".format(masks.shape))

print(boxes[0, 0, 7])
print(masks[2].shape)


def post_pros(confthres, boxes, masks, img_H, img_W):
    numclasses = masks.shape[1]
    numDetection = boxes.shape[2]
    results = []
    for i in range(numDetection):
        box = boxes[0, 0, i]
        mask = masks[i]
        score = box[2]
        if score > confthres:
            left = int(img_W*box[3])
            top = int(img_H*box[4])
            right = int(img_W*box[5])
            bottom = int(img_H*box[6])
            left = max(0, min(left, img_W - 1))
            top = max(0, min(top, img_H - 1))
            right = max(0, min(right, img_W - 1))
            bottom = max(0, min(bottom, img_H - 1))
            result = {"score": score, "classid": int(box[1]), "box": (left, top, right, bottom),
                      "mask": mask[int(box[1])]}
            results.append(result)
    return results


results = post_pros(confThres, boxes, masks, H, W)

# print(result)
'''  print mask matrix for test 
for result in results:
    mask = result["mask"]
    print(mask[2])
'''



def vis_results(maskThres, colors, classes, net, image_path, results):
    img_cv2= cv2.imread(image_path)
    for result in results:
        # box
        left, top, right, bottom = result["box"]
        color1 = random.choice(COLORS_1)
        color1 = [int(c) for c in color1]
        cv2.rectangle(img_cv2,
                      (left, top),
                      (right, bottom),
                      color1, 2)

        classid = result["classid"]
        score = result["score"]
        label = '%.2f' % score
        if classes:
            assert (classid < len(classes))
            label = '%s:%s' % (classes[classid], label)

        label_size, baseline = cv2.getTextSize(
                label, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1)

        top = max(top, label_size[1])
        cv2.rectangle(
            img_cv2,
            (left, top - round(0.4 * label_size[1])),
            (left + round(0.4 * label_size[0]), top + baseline),
            (255, 255, 255), cv2.FILLED)

        cv2.putText(
            img_cv2,
            label,
            (left, top),
            cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 0), 1)

        class_mask = result["mask"]

        class_mask = cv2.resize(class_mask, (right - left + 1, bottom - top + 1))

        mask = (class_mask > maskThres)

        roi = img_cv2[top: bottom + 1, left: right + 1][mask]
        color_index = random.randint(0, len(colors) - 1)
        color = colors[color_index]

        img_cv2[top: bottom + 1, left: right + 1][mask] = (
                [.3 * color[0], .3 * color[1], .3 * color[2]] + 0.7 * roi).astype(np.uint8)

        mask = mask.astype(np.uint8)

        contours, hierachy = cv2.findContours(
            mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        cv2.drawContours(
            img_cv2[top: bottom + 1, left: right + 1],
            contours,
            -1,
            color,
            3,
            cv2.LINE_8,
            hierachy,
            100)

        t, _ = net.getPerfProfile()

        plt.figure(figsize=(10, 8))
        plt.imshow(img_cv2[:, :, ::-1])
        plt.axis("off")
        plt.show()


if __name__ == '__main__':
    print("[INFO]MaskRCNN detection.")
    vis_results(maskThres, COLORS_2, labels, net, image_path, results)
